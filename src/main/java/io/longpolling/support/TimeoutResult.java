package io.longpolling.support;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 超时响应信息封装体.
 */
@Data
public class TimeoutResult {

    private int timeoutSecond;

    private Object result;

    private HttpStatus status;

    public TimeoutResult(int timeoutSecond, Object result, HttpStatus status) {
        this.timeoutSecond = timeoutSecond;
        this.result = result;
        this.status = status;
    }

    public static TimeoutResult newInstance(int timeoutSecond, Object result, HttpStatus status) {
        return new TimeoutResult(timeoutSecond, result, status);
    }


}
