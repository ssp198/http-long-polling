package io.longpolling.support;

/**
 * 请求参数封装.
 */
public class RequestParam {

    private final String resource;
    private final Object param; //自定义

    RequestParam(String resource, Object param) {
        this.resource = resource;
        this.param = param;
    }

    public static RequestParam newInstance(String resource, Object param) {
        return new RequestParam(resource, param);
    }

    public String getResource() {
        return resource;
    }

    public Object getParam() {
        return param;
    }
}
